<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
    use HasFactory;

    protected $fillable = [
        'victim_name',
        'incident_date',
        'location',
        'report_content',
        'image',
        'addressed_to',
        'bullying_type',
        'status',
        'reporter_id',
        'created_at',
        'updated_at',
    ];

    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id');
    }

    public function response()
    {
        return $this->hasMany(Response::class, 'reponse_id');
    }
}
