<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Student;
use Illuminate\Database\QueryException;
use App\Models\Complaint;
use Illuminate\Support\Facades\Auth;

class ComplaintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $complaint = Complaint::paginate(5);
        return view('admin.complaints.index', ['complaint_list' => $complaint]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $students = Student::all();
        return view('admin.complaints.create', ['student_list' => $students]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { {
            $data = $request->validate([
                'victim_name' => 'required',
                'incident_date' => 'required',
                'location' => 'required',
                'bullying_type' => 'required',
                'report_content' => 'required',
                'image' => 'required|file',
                'addressed_to' => 'required',
                'status' => 'required',
                'reporter_id' => 'required',
            ]);

            $path = $request->image->store('public/images');
            $data['image'] = str_replace('public/', '', $path);

            Complaint::create($data);

            return redirect('/admin/complaints');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    { {
            $complaint = Complaint::find($id);
            return view('admin.complaints.detail', ['complaint' => $complaint]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'victim_name' => 'required',
            'incident_date' => 'required',
            'location' => 'required',
            'report_content' => 'required',
            'image' => 'nullable|file',
            'addressed_to' => 'required',
            'bullying_type' => 'required',
            'status' => 'required',
            'reporter_id' => 'required',
        ]);

        if (array_key_exists('image', $data)) {
            $path = $request->image->store('public/image');
            $data['image'] = str_replace('public/', '', $path);
        }
        
        Complaint::where('id', $id)->update($data);

        return redirect('/admin/complaints');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Complaint::destroy($id);
            return redirect('/admin/complaints');
        } catch (QueryException $exc) {
            return redirect('/admin/complaints')
            ->withErrors([
                'msg' => 'complaint ' . $id . ' cannot be deleted because related with other entity'
            ]);
        }
    }
}
