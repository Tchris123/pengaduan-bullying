<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\Complaint;

class ComplaintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $complaint = Complaint::paginate(5);
        return view('student.complaints.index', ['complaint_list' => $complaint]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $students = Student::all();
        return view('student.complaints.create', ['student_list' => $students]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        {
            $data = $request->validate([
                'victim_name' => 'required',
                'incident_date' => 'required',
                'location' => 'required',
                'bullying_type' => 'required',
                'report_content' => 'required',
                'image' => 'required|file',
                'addressed_to' => 'required',
            ]);

            $path = $request->image->store('public/images');
            $data['image'] = str_replace('public/', '', $path);
            $data['reporter_id'] = Auth::user()->student->id;

            $data['status'] = 'not verified';
            Complaint::create($data);

            return redirect('/student/complaints');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    { {
            $complaint = Complaint::find($id);
            return view('student.complaints.detail', ['complaint' => $complaint]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Complaint::destroy($id);
            return redirect('/student/complaints');
        } catch (QueryException $exc) {
            return redirect('/student/complaints')
            ->withErrors([
                'msg' => 'complaint ' . $id . ' cannot be deleted because related with other entity'
            ]);
        }
    }
}
