<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Student;
use App\Models\Complaint;
use App\Models\Response;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {


        $user_6 = User::create([
            'name' => 'Kamda',
            'username' => 'bull',
            'password' => Hash::make('Test123'),
            'phone' => '089555334988',
            'Level' => 'admin',
        ]);

        $user_7 = User::create([
            'name' => 'Cika',
            'username' => 'mall',
            'password' => Hash::make('Test123'),
            'phone' => '089555334819',
            'Level' => 'staff',
        ]);



        $user_1 = User::create([
            'name' => 'Agustinus',
            'username' => 'Tinus',
            'password' => Hash::make('Test123'),
            'phone' => '089555334777',
            'Level' => 'student',
        ]);

        $student_1 = Student::create([
            'nis' => '201920001',
            'class' => '10',
            'gender' => 'male',
            'user_id' => $user_1->id,
        ]);

        $complaint_1 = Complaint::create([
            'victim_name' => 'Mahmud',
            'incident_date' => '2019-10-09',
            'location' => 'Ruang 7',
            'report_content' => 'Saat saya bertemu dengan Siti, tiba-tiba Siti mengejek saya hitam',
            'image' => 'bukti',
            'addressed_to' => 'Siti',
            'bullying_type' => 'verbal',
            'status' => 'not verified',
            'reporter_id' => $student_1->id,
        ]);

        $response_1 = Response::create([
            'complaint_id' => $complaint_1->id,
            'response' => 'baik, terima kasih atas laporan anda kami akan menindak lanjuti kasus tersebut, mohon ditunggu terima kasih',
            'staff_id' => $user_7->id,
        ]);


        $user_2 = User::create([
            'name' => 'Somat',
            'username' => 'mamat',
            'password' => Hash::make('Test1234'),
            'phone' => '089555334999',
            'Level' => 'student',
        ]);

        $student_2 = Student::create([
            'nis' => '201920002',
            'class' => '10',
            'gender' => 'male',
            'user_id' => $user_2->id,
        ]);

        $complaint_2 = Complaint::create([
            'victim_name' => 'Abdul',
            'incident_date' => '2019-10-08',
            'location' => 'Ruang 8',
            'report_content' => 'Saat saya keluar dari kamar mandi tiba-tiba tas saya disembunyikan oleh Fajar',
            'image' => 'bukti',
            'addressed_to' => 'Fajar',
            'bullying_type' => 'physical',
            'status' => 'not verified',
            'reporter_id' => $student_2->id,
        ]);

        $response_2 = Response::create([
            'complaint_id' => $complaint_2->id,
            'response' => 'baik, terima kasih atas laporan anda kami akan menindak lanjuti kasus tersebut, mohon ditunggu terima kasih',
            'staff_id' => $user_7->id,
        ]);


        $user_3 = User::create([
            'name' => 'Jaka',
            'username' => 'Jake',
            'password' => Hash::make('Test12345'),
            'phone' => '089555334333',
            'Level' => 'student',
        ]);

        $student_3 = Student::create([
            'nis' => '201920003',
            'class' => '9',
            'gender' => 'male',
            'user_id' => $user_3->id,
        ]);

        $complaint_3 = Complaint::create([
            'victim_name' => 'Somat',
            'incident_date' => '2019-10-21',
            'location' => 'Ruang 8',
            'report_content' => 'Ketika saya mengupload foto di sosial media, Kakang mengejek saya gemuk',
            'image' => 'bukti',
            'addressed_to' => 'Kakang',
            'bullying_type' => 'cyber',
            'status' => 'not verified',
            'reporter_id' => $student_3->id,
        ]);

        $response_3 = Response::create([
            'complaint_id' => $complaint_3->id,
            'response' => 'baik, terima kasih atas laporan anda kami akan menindak lanjuti kasus tersebut, mohon ditunggu terima kasih',
            'staff_id' => $user_6->id,
        ]);


        $user_4 = User::create([
            'name' => 'Mamat',
            'username' => 'abdul',
            'password' => Hash::make('Test123456'),
            'phone' => '089555334888',
            'Level' => 'student',
        ]);

        $student_4 = Student::create([
            'nis' => '201920004',
            'class' => '8',
            'gender' => 'male',
            'user_id' => $user_4->id,
        ]);

        $complaint_4 = Complaint::create([
            'victim_name' => 'Eko',
            'incident_date' => '2019-10-16',
            'location' => 'Ruang 19',
            'report_content' => 'Rizki mengejek saya dengan kata-kata kasar yang membuat saya sakit hati',
            'image' => 'bukti',
            'addressed_to' => 'Rizki',
            'bullying_type' => 'verbal',
            'status' => 'not verified',
            'reporter_id' => $student_4->id,
        ]);

        $response_4 = Response::create([
            'complaint_id' => $complaint_4->id,
            'response' => 'baik, terima kasih atas laporan anda akan kami proses pada kasus tersebut, mohon ditunggu terima kasih',
            'staff_id' => $user_7->id,
        ]);


        $user_5 = User::create([
            'name' => 'Putri',
            'username' => 'Put',
            'password' => Hash::make('Test1234567'),
            'phone' => '089555334799',
            'Level' => 'student',
        ]);

        $student_5 = Student::create([
            'nis' => '201920005',
            'class' => '11',
            'gender' => 'female',
            'user_id' => $user_5->id,
        ]);

        $complaint_5 = Complaint::create([
            'victim_name' => 'Chika',
            'incident_date' => '2019-10-18',
            'location' => 'Ruang 17',
            'report_content' => 'putri mengejek saya dengan keras marga keluarga saya',
            'image' => 'bukti',
            'addressed_to' => 'putri',
            'bullying_type' => 'verbal',
            'status' => 'not verified',
            'reporter_id' => $student_5->id,
        ]);

        $response_5 = Response::create([
            'complaint_id' => $complaint_1->id,
            'response' => 'baik, terima kasih atas laporan anda kami akan segera menindak lanjuti kasus tersebut, mohon ditunggu terima kasih',
            'staff_id' => $user_6->id,
        ]);


        
    }
};
