@extends('app')

@section('content')

    <body>
        <div class="container">
            <div class="penjelasan" id="penjelasan">
                <h2 style="color: darkblue">Apa itu Bullying?</h2>
                <p>Bullying adalah perilaku agresif yang dilakukan berulang-ulang dan sengaja oleh satu atau beberapa orang
                    terhadap orang lain yang dianggap lebih lemah. Perilaku ini dapat terjadi di berbagai tempat, seperti di
                    sekolah, di tempat kerja, dan di lingkungan masyarakat.</p>
            </div>
            <div class="jenis" id="jenis">
                <h2 style="color: darkblue">Jenis-Jenis Bullying</h2>
                <p>Bullying dapat terjadi dalam beberapa bentuk, jenis-jenis bullying yaitu:</p>
                <ul>
                    <li>Verbal bullying: menggunakan kata-kata/ucapan yang menyakitkan untuk menyakiti orang lain</li>
                    <li>Fisik bullying: menggunakan kekerasan fisik untuk menyakiti orang lain</li>
                    <li>Sosial bullying: mengecualikan/membedakan orang lain dari kegiatan sosial dan membuat mereka merasa
                        terisolasi</li>
                    <li>Cyberbullying: menggunakan media sosial dan teknologi untuk menyakiti orang lain secara online,
                        contohnya berkomentar buruk di postingan seseorang.</li>
                </ul>
            </div>
            <div class="sanksi" id="sanksi">
                <h2 style="color: darkblue">Sanksi Bullying</h2>
                <p>Bullying memiliki sanksi yang cukup serius jika kamu melakukannya, berikut sanksi-sanksinya:</p>
                <ul>
                    <li>Teguran
                        Sanksi awal yang biasanya diberikan adalah teguran atau peringatan verbal dari guru atau staf
                        sekolah.
                        Teguran ini bertujuan untuk memberikan kesempatan kepada pelaku untuk memperbaiki perilakunya.</li>

                    <li>Pembatasan kegiatan atau keikutsertaan dalam acara
                        Pelaku bullying dapat diberikan sanksi berupa pembatasan kegiatan atau keikutsertaannya dalam
                        acara-acara di
                        sekolah atau tempat lainnya.
                    </li>
                    <li>Penghentian keanggotaan di organisasi
                        Jika pelaku bullying adalah anggota dari suatu organisasi atau klub di sekolah, maka sanksi yang
                        diberikan
                        dapat berupa penghentian keanggotaannya.</li>

                    <li>Tindakan disiplin atau hukuman
                        Jika pelaku bullying terus melakukan tindakan yang merugikan atau mengganggu lingkungan sekolah,
                        maka
                        sanksi
                        yang diberikan dapat berupa tindakan disiplin atau hukuman, seperti dikeluarkan dari sekolah atau
                        institusi,
                        atau diberikan hukuman lain yang lebih berat.</li>
                </ul>
            </div>
            <div class="dampak" id="dampak">
                <h2 style="color: darkblue">Dampak Bullying</h2>
                <p>Bullying memiliki dampak yang serius pada kesehatan mental dan fisik korban. Beberapa dampak yang
                    mungkin terjadi yaitu:</p>
                <ul>
                    <li>Depresi</li>
                    <li>Gangguan makan</li>
                    <li>Gangguan tidur</li>
                    <li>Kehilangan minat pada aktivitas yang biasa dinikmati</li>
                    <li>Kehilangan percaya diri</li>
                </ul>
            </div>
        </div>
    </body>
@endsection
