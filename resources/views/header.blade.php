<?php
if (auth()->user()) {
    $level = auth()->user()->level;
} else {
    $level = '';
}
?>

<header class="p-3 mb-3 border-bottom d-print-none" id="navigation-bar">
    <div class="container d-flex align-items-center">
        <a href="#" class="d-flex align-items-center me-3 text-decoration-none"></a>
        <img src="/img/logo.png" style="width: 150px">
        <span class="fs-4 ms-5 me-5"></span>
        <ul class="nav me-auto" style="margin-left: 40px;">
                <li><a href="/" class="nav-link">BERANDA</a></li>
            <div class="dropdown">
                <li><a href="/informasi" class="nav-link" style="margin-left: 43px;">INFORMASI</a></li>
            </div>
            <li><a href="/report" class="nav-link" style="margin-left: 47px;">PENGADUAN</a></li>
            @if (auth()->user() == null)
                <a href="/login"><button class="btn btn-success" style="margin-left: 225px;">LOGIN</button></a>
            @endif
        </ul>
        @if (auth()->user())
            <a href="#" class="dropdown-toggle text-decoration-none" data-bs-toggle="dropdown">
                <i class="bi bi-person-circle" style="font-size: 35px"></i>
            </a>

            <ul class="dropdown-menu">
                <li><a href="#" class="dropdown-item">Halo, {{ auth()->user()->username }}</a></li>
                @if (auth()->user()->level == 'student')
                <li><a href="/student/complaints" class="dropdown-item">Laporan Saya</a></li>
                <li><a href="/student/responses" class="dropdown-item">Tanggapan</a></li>
                @endif
                @if (auth()->user()->level == 'admin')
                <li><a href="/admin/users" class="dropdown-item">Users</a></li>
                <li><a href="/admin/students" class="dropdown-item">Students</a></li>
                <li><a href="/admin/complaints  " class="dropdown-item">Complaints</a></li>
                <li><a href="/admin/responses" class="dropdown-item">Responses</a></li>
                <li><a href="/admin/complaints-report" class="dropdown-item">Generate laporan</a></li>
                @endif
                @if (auth()->user()->level == 'staff')
                <li><a href="/staff/students" class="dropdown-item">Students</a></li>
                <li><a href="/staff/complaints  " class="dropdown-item">Complaints</a></li>
                <li><a href="/staff/responses" class="dropdown-item">Responses</a></li>
                <li><a href="/admin/complaints-report" class="dropdown-item">Generate laporan</a></li>
                @endif
                <li><a href="/logout" class="dropdown-item">Keluar</a></li>
        @endif
        </ul>
    </div>
</header>
