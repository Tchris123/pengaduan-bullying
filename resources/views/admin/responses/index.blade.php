@extends('app')

@section('content')
    <div class="container">
        <h1>DATA RESPONSE</h1>
        <p>{{ $response_list->links() }}</p>
        <table class="table">
            <thead>
                <tr>
                    <th>NO.</th>
                    <th>COMPLAINT ID</th>
                    <th>RESPONSE</th>
                    <th>STAFF ID</th>
                    <th>ACTION</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($response_list as $response)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $response->complaint_id }}</td>
                        <td>{{ $response->response }}</td>
                        <td>{{ $response->staff_id }}</td>
                        <td>
                            <a href="/admin/responses/{{ $response->id }}" class="btn btn-info">Detail</a>
                            <a href="#" class="btn btn-danger" data-bs-toggle="modal"
                                data-bs-target="#modal-{{ $response->id }}">Delete</a>
                        </td>
                    </tr>

                    <div class="modal fade" id="modal-{{ $response->id }}" tabindex="-1" >
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Konfirmasi</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                                </div>
                                <div class="modal-body">
                                    <p>Response dengan ID {{ $response->id }} akan dihapus.</p>
                                    <p>Lanjutkan?</p>
                                </div>
                                <div class="modal-footer">
                                    <form action="/admin/responses/{{ $response->id }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                        <button type="button" calss="btn btn-secondary"
                                        data-bs-dismiss="modal">Batal</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </tbody>
        </table>
        <a href="/admin/responses/create" class="btn btn-success">Tambah</a>
    </div>
@endsection
