@extends('app')

@section('content')
    <div class="container">
        <div class="belakang">
            <form action="/student/complaints" method="POST" enctype="multipart/form-data" id="student-create">
                @csrf
                <h1 style="margin-top: -70px; margin-left: -50px;">TAMBAH COMPLAINT</h1>
                <div class="d-flex">
                    <div class="p-2 mt-5 w-75">
                        <img src="/img/lapor.png" id="gambar-report">
                    </div>
                    <div class="p-5 mt-3 ms-5 flex-shrink-1 w-75" style="margin-right: -35px;">
                        <div class="row flex-column">
                            <div class="col-3 mb-3 w-75">
                                <label for="victim_name" class="form-label">Victim name</label>
                                <input type="text" class="form-control" id="victim_name" name="victim_name">
                            </div>
                        </div>

                        <div class="row flex-column">
                            <div class="col-3 mb-3 w-75">
                                <label for="incident_date" class="form-label">Incident date</label>
                                <input type="date" class="form-control" id="incident_date" name="incident_date">
                            </div>
                        </div>

                        <div class="row flex-column">
                            <div class="col-3 mb-3 w-75">
                                <label for="location" class="form-label">Location</label>
                                <input type="text" class="form-control" id="location" name="location">
                            </div>
                        </div>

                        <div class="row flex-column">
                            <div class="col-3 mb-3 w-75">
                                <label for="report_content" class="form-label">Report conten</label>
                                <input type="text" class="form-control" id="report_content" name="report_content">
                            </div>
                        </div>

                        <div class="col-4 mb-3 w-75">
                            <label for="image" class="form-label">Image</label>
                            <input type="file" class="form-control" id="image" name="image"
                                accept="image/png.image/jpeg">
                        </div>

                        <div class="row flex-column">
                            <div class="col-3 mb-3 w-75">
                                <label for="addressed_to" class="form-label">Addressed to</label>
                                <input type="text" class="form-control" id="addressed_to" name="addressed_to">
                            </div>
                        </div>

                        <div class="row flex-column">
                            <div class="col-3 mb-3 w-75">
                                <label for="bullying_type" class="form-label">Bullying type</label>
                                <select name="bullying_type" class="form-select">
                                    @foreach (['verbal', 'physical', 'sosial', 'sexual', 'cyber'] as $item)
                                        <option value="{{ $item }}">
                                            {{ $item }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-success" style="margin-left: 510px; margin-top: -70px;">Simpan</button>
                <button type="reset" class="btn btn-danger" style="margin-top: -70px;">Reset</button>
            </form>
        </div>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <div class="text-danger">{{ $error }}</div>
            @endforeach
        @endif
    </div>
@endsection
