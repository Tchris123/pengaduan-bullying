@extends('app')

@section('content')
    <div class="container">
        <h1>DETAIL COMPLAINT</h1>
        <form action="/admin/complaints/{{ $complaint->id }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="victim_name" class="form-label">Victim name</label>
                    <input type="text" class="form-control" id="victim_name" name="victim_name" value="{{ $complaint->victim_name }}">
                </div>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="incident_date" class="form-label">Incident_date</label>
                    <input type="date" class="form-control" id="incident_date" name="incident_date" value="{{ $complaint->incident_date }}">
                </div>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="location" class="form-label">Location</label>
                    <input type="text" class="form-control" id="location" name="location" value="{{ $complaint->location }}">
                </div>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="report_content" class="form-label">Report_content</label>
                    <input type="text" class="form-control" id="report_content" name="report_content" value="{{ $complaint->report_content }}">
                </div>
            </div>

            <div class="col-4 mb-3">
                <label for="image" class="form-label">Image</label>
                <input type="file" class="form-control" id="image" name="image"
                    accept="image/png.image/jpeg">
                    <img src="{{ asset('storage/' . $complaint->image) }}" style="width: 300px" srcset="">
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="addressed_to" class="form-label">Addressed to</label>
                    <input type="text" class="form-control" id="addressed_to" name="addressed_to" value="{{ $complaint->addressed_to }}">
                </div>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="bullying_type" class="form-label">Bullying type</label>
                    <select name="bullying_type" class="form-select">
                        @foreach (['verbal', 'physical', 'sosial', 'sexual', 'cyber'] as $item)
                            <option value="{{ $item }}" {{ $complaint->bullying_type == $item ? 'selected' : '' }}>
                            {{ $item }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="status" class="form-label">Status</label>
                    <select name="status" class="form-select">
                        @foreach (['not verified', 'verified', 'rejected', 'process', 'finish'] as $item)
                            <option value="{{ $item }}" {{ $complaint->status == $item ? 'selected' : '' }}>
                            {{ $item }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="reporter_id" class="form-label">Reporter id</label>
                    <input type="text" class="form-control" id="reporter_id" name="reporter_id" value="{{ $complaint->reporter_id }}">
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
        </form>
        @if ($errors->any())
        @foreach ($errors->all() as $error)
            <p class="text-danger">{{ $error }}</p>
        @endforeach
    @endif
    </div>
@endsection
