@extends('app')

@section('content')
    <div class="container">
        <h1>DATA COMPLAINT</h1>
        <p>{{ $complaint_list->links() }}</p>
        <table class="table">
            <thead>
                <tr>
                    <th>NO.</th>
                    <th>VICTIM NAME</th>
                    <th>INCIDENT DATE</th>
                    <th>LOCATION</th>
                    <th>REPORT CONTENT</th>
                    <th>ADDRESSED TO</th>
                    <th>BULLYING TYPE</th>
                    <th>STATUS</th>
                    <th>ACTION</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($complaint_list as $complaint)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $complaint->victim_name }}</td>
                        <td>{{ $complaint->incident_date }}</td>
                        <td>{{ $complaint->location }}</td>
                        <td>{{ $complaint->report_content }}</td>
                        <td>{{ $complaint->addressed_to }}</td>
                        <td>{{ $complaint->bullying_type }}</td>
                        <td>{{ $complaint->status }}</td>
                        
                        <td>
                            <a href="/admin/complaints/{{ $complaint->id }}" class="btn btn-info">Detail</a>
                            <a href="#" class="btn btn-danger" data-bs-toggle="modal"
                                data-bs-target="#modal-{{ $complaint->id }}">Delete</a>
                        </td>
                    </tr>

                    <div class="modal fade" id="modal-{{ $complaint->id }}" tabindex="-1" >
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Konfirmasi</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                                </div>
                                <div class="modal-body">
                                    <p>User dengan ID {{ $complaint->id }} akan dihapus.</p>
                                    <p>Lanjutkan?</p>
                                </div>
                                <div class="modal-footer">
                                    <form action="/admin/complaints/{{ $complaint->id }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                        <button type="button" calss="btn btn-secondary"
                                        data-bs-dismiss="modal">Batal</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </tbody>
        </table>
        <a href="/admin/complaints/create" class="btn btn-success">Tambah</a>
    </div>
@endsection
