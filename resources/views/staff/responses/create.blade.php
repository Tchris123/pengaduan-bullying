@extends('app')

@section('content')
    <div class="container">
        <h1>TAMBAH RESPONSE</h1>
        <form action="/staff/responses" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="complaint_id" class="form-label">Complaint id</label>
                    <input type="text" class="form-control" id="complaint_id" name="complaint_id">
                </div>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="response" class="form-label">Response</label>
                    <input type="text" class="form-control" id="response" name="response">
                </div>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="staff_id" class="form-label">Staff id</label>
                    <input type="text" class="form-control" id="staff_id" name="staff_id">
                </div>
            </div>


            <button type="submit" class="btn btn-success">Simpan</button>
            <button type="reset" class="btn btn-danger">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <div class="text-danger">{{ $error }}</div>
            @endforeach            
        @endif
    </div>
@endsection
