@extends('app')

@section('content')
    <div class="container">
        <h1>DETAIL RESPONSE</h1>
        <form action="/staff/responses/{{ $response->id }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="complaint id" class="form-label">Complaint id</label>
                    <input type="text" class="form-control" id="complaint id" name="complaint id" value="{{ $response->complaint_id }}">
                </div>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="response" class="form-label">Response</label>
                    <input type="text" class="form-control" id="response" name="response" value="{{ $response->response }}">
                </div>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="staff id" class="form-label">Staff id</label>
                    <input type="text" class="form-control" id="staff id" name="staff id" value="{{ $response->staff_id }}">
                </div>
            </div>


            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
        </form>
        @if ($errors->any())
        @foreach ($errors->all() as $error)
            <p class="text-danger">{{ $error }}</p>
        @endforeach
    @endif
    </div>
@endsection
