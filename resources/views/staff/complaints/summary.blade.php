@extends('app')

@section('content')
    <div class="container">
        <h1>Report content</h1>
        <form action="/staff/complaints-report" method="GET">
            <div class="row">
                <label for="start" class="col-1 col-form-label">Dari</label>
                <div class="col-2">
                    <input type="date" class="form-control" id="start" name="start" value="{{ $start }}">
                </div>
                <label for="end" class="col-1 col-form-label">Sampai</label>
                <div class="col-2">
                    <input type="date" class="form-control" id="end" name="end" value="{{ $end }}">
                </div>
                <div class="col-1">
                    <button type="submit" class="btn btn-success">Cari</button>
                </div>
            </div>
        </form>
        <table class="table">
            <thead>
                <tr>
                    <th>NO.</th>
                    <th>VICTIM NAME</th>
                    <th>INCIDENT DATE</th>
                    <th>LOCATION</th>
                    <th>BULLYING_TYPE</th>
                    <th>ADDRESSED TO</th>
                    <th>STATUS</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($complaint_list as $complaint)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $complaint->victim_name}}</td>
                        <td>{{ $complaint->incident_date }}</td>
                        <td>{{ $complaint->location }}</td>
                        <td>{{ $complaint->bullying_type }}</td>
                        <td>{{ $complaint->addressed_to }}</td>
                        <td>{{ $complaint->status }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <button type="button" class="btn btn-danger" onclick="window.print()">
            Print
        </button>
    </div>
@endsection