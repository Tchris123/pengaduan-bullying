@extends('app')

@section('content')
    <div class="container">
        <h1>DATA STUDENT</h1>
        <p>{{ $student_list->links() }}</p>
        <table class="table">
            <thead>
                <tr>
                    <th>NO.</th>
                    <th>NIS</th>
                    <th>USER NAME</th>
                    <th>CLASS</th>
                    <th>GENDER</th>
                    <th>ACTION</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($student_list as $student)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $student->nis }}</td>
                        <td>{{ $student->user->name }}</td>
                        <td>{{ $student->class }}</td>
                        <td>{{ $student->gender }}</td>
                        <td>
                            <a href="/staff/students/{{ $student->id }}" class="btn btn-info">Detail</a>
                            <a href="#" class="btn btn-danger" data-bs-toggle="modal"
                                data-bs-target="#modal-{{ $student->id }}">Delete</a>
                        </td>
                    </tr>
                    <div class="modal fade" id="modal-{{ $student->id }}" tabindex="-1" >
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Konfirmasi</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                                </div>
                                <div class="modal-body">
                                    <p>Student dengan ID {{ $student->id }} akan dihapus.</p>
                                    <p>Lanjutkan?</p>
                                </div>
                                <div class="modal-footer">
                                    <form action="/staff/students/{{ $student->id }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                        <button type="button" calss="btn btn-secondary"
                                        data-bs-dismiss="modal">Batal</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </tbody>
        </table>
        <a href="/staff/students/create" class="btn btn-success">Tambah</a>
    </div>
@endsection
