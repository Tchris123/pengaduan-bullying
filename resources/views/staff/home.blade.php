@extends('app')

@section('content')

    <body>
        <div class="halaman-home">
            <div class="container">
                <div class="bg-home" id="bg">
                    <h1 id="judul">Selamat Datang di <br> Avoid Bullying</h1>
                    <p id="text">Avoid Bullying merupakan sebuah layanan berupa website
                        yang gunanya <br> untuk melaporkan setiap kasus-kasus bullying yang terjadi di Sekolah Gracia.</p>
    
    
                    <br>
                    <p id="text2">Jangan pernah segan untuk melapor kepada kami atas kasus bullying <br> yang pernah terjadi!
                    </p>
                    <a href="/report"><button id="tombol"> Tambah laporan</button></a>
                </div>
            </div>
        </div>
    </body>
@endsection
