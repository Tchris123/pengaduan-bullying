<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="footer, address, phone, icons" />

	<link rel="stylesheet" href="style.css">
	
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

	<link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">

</head>
	<body>

		<!-- The content of your page would go here. -->

		<footer class="footer-distributed d-print-none">

			<div class="footer-left">
				<h3><span>AVOID BULLYING</span></h3>
                <p class="footer-company-about">
                    Avoid Bullying merupakan sebuah layanan berupa website
                    yang gunanya untuk melaporkan setiap kasus-kasus bullying yang terjadi di Sekolah Gracia.</p>
				<div class="footer-icons">
					<a href="#"><i class="fa fa-instagram" id="ig"></i></a>
					<a href="#"><i class="fa fa-youtube" id="yt"></i></a>
				</div>
			</div>

			<div class="footer-center">
				<div>
					<i class="fa fa-map-marker"></i>
					  <p><span>Jl. Cibadak Gg. Sereh,
						 no. 26 kec. Astanaanyar</span>
						Kota Bandung, Jawa Barat</p>
				</div>

				<div>
					<i class="fa fa-phone"></i>
					<p>(022) 6014810</p>
				</div>
				<div>
					<i class="fa fa-envelope"></i>
					<p><a href="https://www.samuelpasaribu.com">smkgraciabandung@gmail.com</a></p>
				</div>
			</div>
			<div class="footer-right" style="margin-top: -12px; margin-left: 10px">
				<p class="footer-links">
					<a href="/"> • Home</a>
					<br>
					<a href="/informasi"> • Informasi</a>
					<br>
					<a href="/report"> • Pengaduan</a>
                    <br>
				</p>

				<p class="footer-company-name">© 2023 Avoid Bullying</p>
			</div>
		</footer>
	</body>
</html>